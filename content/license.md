+++
categories = []
date = "2016-03-27T09:46:16-03:00"
description = ""
keywords = []
title = "license"

+++

Este site está sob licença CC-BY-SA-2.0.

Você precisa informar a origem e criar conteúdo com a mesma licença para poder usá-lo.

* https://creativecommons.org/licenses/by-sa/2.0/br/

[s]<br>
Guto
