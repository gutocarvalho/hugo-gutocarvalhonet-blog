+++
categories = ['puppet','meetup','planeta']
date = "2016-05-10T09:55:11-03:00"
title = "Meetup Puppet-BR em BSB no dia 18-05"

+++

Foi marcado um Meetup Puppet-BR em Brasília no dia 18 de Maio, será no
edifício Parque Cidade Corporate, vai começar às 18:30.

Este será o terceiro encontro técnico do ano, foco nas atividades práticas.

Faça sua inscrição, temos apenas 50 vagas.

http://www.meetup.com/pt-BR/puppet-br/events/230816720/

## Programação:

### 1. Intro CGONF & Puppet 4

Palestrante: Rafael Sales & Guto Carvalho<br>
Tempo: 40 min

- Introdução rápida a gerência de configurações;
- Introdução ao Puppet 4.

### 2. Projeto PCP

Palestrante: Taciano Tres & Guto Carvalho<br>
Tempo: 20 min

- Apresentação do projeto PCP;
- Demo de instalação do PCP.

### 3. Oficina de desenvolvimento de módulos e fatos Puppet

Palestrante: Rafael Sales & Guto Carvalho<br>
Tempo: 40 minutos

- Demo de desenvolvimento de módulos;
- Demo de desenvolvimento de fatos.

### 4. Integração do Puppet com Vagrant

Palestrante: Taciano Tres<br>
Tempo: 20 minutos

- Introdução rápida ao Vagrant;
- Uso do Provider Puppet Apply;
- Uso do Provider Puppet Agent;
- Demo de integração de Puppet com Vagrant.

### 5. Testes de código Puppet

Palestrante: Adriano Vieira & Guto Carvalho<br>
Tempo: 30 minutos

- Porque testar?
- Puppet Parser Validate
- Puppet Lint
- Entendendo teste unitário;
- Entendendo teste de aceitação;
- Introdução rápida ao Rspec-Puppet;
- Introdução rápida ao ServerSpec;
- Introdução rápida ao Beaker;
- Demo de desenvolvimento de teste unitário para o módulo que criamos;
- Demo de desenvolvimento de teste de aceitação.

### 6. Integração do Puppet com GitLab

Palestrantes: Douglas Andrade e Guto Carvalho<br>

- Introdução rápida ao GitLab;
- Demo de instalação do GitLab;
- Demo GitLab CI;
- Integrações com Puppet (VCSREPO);
- Integrações com Puppet (R10k).

[s]<br>
Guto
