+++
categories = ['devops','planeta']
date = "2016-06-03T12:25:25-03:00"
description = ""
keywords = []
title = "Vagas DevOps fazem sentido? Entenda!"

+++

Não, não fazem!

E quem diz isso são apenas os criadores do movimento DevOps e core members do DevOpsDays, dentre eles Patrick Debois, Damon Edwards, John Willis e outros.

Hoje o mundo está tomado por vagas Arquiteto DevOps, Engenheiro DevOps,  Especialista DevOps e a informação sobre as vagas é muito ruim.

Como DevOps é essencialmente uma CULTURA, fica difícil você colocar anúncio para contratar uma cultura, um modelo, um movimento, uma metodologia.

No Brasil especialmente a confusão sobre DevOps é terrível, tem gente que acha que DevOps é um programador que sabe infra, ou que é um sysadmin que sabe programar. Alô, DevOps não é um cargo, não é uma pessoa, DevOps é uma CULTURA.

Também não existe time DevOps, se alguém usa esse termo para se referir ao seu time ele realmente não entendeu nada sobre DevOps.

Outra confusão que vejo com muita frequência é sobre CI (Continuous Integration) e CD (Continuos Delivery/Deployment) serem DevOps ou não. 

Tem gente que fala:

    - Amanhã vou em um evento DevOps, discutir CD em plataforma Java e .Net.

Ok, dentro da Cultura DevOps você pode sim utilizar CD e CI, certamente vai usar, mas isso é uma ferramenta/metodologia que a você traz para ajudar a resolver problemas culturais em uma organização, isso sozinho não pode ser considerado DevOps, na verdade é injusto até pegar algo tão amplo, tão importante e julgar como se fosse apenas um pipeline de commit, build, test e deploy.

Sim DevOps foi criado para entregar software melhor, mais rápido, com menor risco e de forma automatizada e controlada, ele é fundamentado nisso, mas antes disso tudo, a Cultura estabelece uma série de eixos para resolver os problemas internos da organização que a IMPEDEM de entregar softwares melhores, mais rápidos, com menor risco de forma automatizada, antes da tecnologia vem o problema a ser assumido, resolvido, uma cultura a ser mudada para que ai sim todas as tecnologias e métodos possa ajudar.

CULTURA, guarde essa palavra.

## Como contratar alguém que saiba essa cultura?

Mas eu preciso contratar uma pessoa seja um entusiasta da cultura DevOps, eu quero começar a transformar a minha organização, esse cara será o agente de mudanças, qual o nome dele?

    CONSULTOR
      
Tá, mas eu quero alguém técnico para trabalhar já bebendo na fonte dessa cultura, e ai como divulgo?  

Segue um exemplo:

>
> Nome da vaga (sem devops no meio)
> 
> Estamos buscando um profissional que consiga trabalhar em times multidisciplinares, que tenha sólidos conhecimentos de programacao na plataforma/linguagem X, e bons fundamentos de sistemas operacionais e redes, que tenha condições de trabalhar com metodos ágeis, com processos e tecnologias de automação. Este profissional deve ter facilidade para adaptar metodos ágeis para uso interno do seu time e de suas atividades.

> Procuramos essencialmente profissionais que consigam se relacionar bem com o seu time, que saibam fazer parte de um time, que respeitem o time, que saiba dividir e compartilhar responsabilidades com o time, que goste de estudar e aprender novas tecnologias e que goste de compartilhar o seu conhecimento.

> Precisamos de profissionais que entendam que sua função é fazer com que o negócio da organização flua, ou seja, o foco do trabalho é oferecer suporte e sustentação as necessidades das pessoas que estão pensando, criando, escrevendo, desenvolvendo e publicando os produtos para atender aos clientes desta organização. 

> Nesta organização enxergamos a TI como uma unidade orgânica composta por pessoas, as pessoas são importantes para nós, nosso entendimento é que a TI é um time monolítico que compartilha seus sucessos e aprendizados.

> Queremos que você agregue valor ao nosso time e a nossa organização, e queremos que a organização agrege valor a você.

> Tecnologias com as quais trabalhamos:
> 
- Linguagens
- Sistemas operacionais
- Serviços
- Plataformas de desenvolvimento
- Plataformas de automação
- Plataformas de monitoramento
- Plataformas de segurança
- Plataformas de nuvem e virtualização
- Plataformas de armazenamento
- Plataformas de rede 

> Métodos que utilizamos em nossos times:
> 
- Método A
- Método B
- Método C

> O que esperamos de você?
> 
- Esperamos que nos ajude a identificar as melhores tecnologias que possam ser utilizadas por novos produtos
- Esperemos que nos ajude a identificar tecnologias que possam melhorar a performance de produtos existentes
- Esperamos que nos ajude a acompanhar a performance e o funcionamento das aplicações
- Esperamos que nos ajude a melhorar nossos processos de provisionamento de vms e containers
- Esperamos que nos ajude a melhorar e agilizar o processo e o tempo necessário para criação de novos ambientes
- Esperamos que nos ajude a oferecer mecanismos de autoserviço entregando recursos diretamente aos desenvolvedores
- Esperamos que nos ajude a automatizar e otimizar nossa infra ao máximo
- Esperamos que nos ajude a registrar mudanças e eventos, gerando relatórios que possibilitem auditoria se preciso
- Esperamos que nos ajude a manter e evoluir nosso processo de deploy para que  possamos entregar sempre e entregar rápido
- Esperamos que você possa ir além, propondo, criando, mudando, construindo e evoluindo junto conosco.

> Se você acha que as características necessárias para participar de nossos projetos e de nosso time, entre em contato!

Veja que é muito mais simples e claro dizer o que você espera da pessoa, usando a cultura DevOps como referência, sem necessáriamente
enfiar o nome DevOps no meio da vaga.

Antes que perguntem sobre sysadmin programar, sim, todo o profissional de TI deveria saber programar, isso não é exatamente Rocket Science, é uma qualidade fundamental para qualquer tipo de vaga de TI hoje em dia, principalmente se a organização se fundamenta pela cultura DevOps que tem automação como um dos seus pilares (CAMS/ICE).

É sempre bom lembrar que saber programar e ser um desenvolvedor ou engenheiro de software são coisas bem diferentes, normalmente não é isso que as pessoas estão buscando quando divulgam esse tipo de vaga. 

Se precisa de um desenvolvedor, divulgar isso não tem mistério.

Puxa eu não preciso de nada disso, minha organização tem várias hierarquias, é bem vertical, engessada, tem vários silos, e a galera não vai topar isso ai, blá, blá, blá.... ok, então seu negócio não tem nada a ver com DevOps, talvez o que você esteja procurando seja um profissional de **infra** que use métodos **ágeis**, alguém que saiba aplicar métodos + automação em um silo como por exemplo uma equipe de operação ou infraestrutura, ele conseguirá nesse caso melhorar processos ali, naquele time, e fazer uma transformação naquela realidade local, isso também é possível.

A isso temos (Eu e Miguel Filho) dado o nome de **infra ágil**, que é uma necessidade e característica mais próxima do que temos visto no Brasil, seja em governo, seja em iniciativa privada.

Eu falarei mais disso em outro post ou em outro blog ;)

Espero ter ajudado.

[s]<br>
Guto