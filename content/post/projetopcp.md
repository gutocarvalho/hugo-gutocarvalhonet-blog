+++
categories = ['puppet','vagrant','github','planeta']
date = "2016-03-27T14:14:16-03:00"
description = ""
keywords = []
title = "Projeto PCP"

+++

Lancei em parceria com o Miguel Filho o projeto PCP - Puppet Community Platform.

O projeto PCP tem o objetivo de fornecer uma plataforma Puppet 4 completa,
com Puppet Server, Puppet Agent, PuppetDB, Mcollective e Puppet Explorer.
Tudo é instalado e integrado de forma automatizada.

Esta plataforma é ideal para testes, avaliações da tecnologia Puppet e desenvolvimento
de módulos e fatos para o Puppet 4.

Acesse o projeto:

* http://github.com/gutocarvalho/pcp

Você pode contribuir enviando PR (Pull Request).

Todo são bem vindos para ajudar.

[s]<br>
Guto
