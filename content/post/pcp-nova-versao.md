+++
categories = ['puppet','vagrant','github','planeta']
date = "2016-05-10T11:36:59-03:00"
title = "Foi liberado o PCP 1.0.2"
+++

O projeto PCP está em casa nova e com nova versão. Movemos o projeto para a organização Puppet-BR no github para facilitar a contribuição. Agora a documentação está toda em inglês o que facilita o uso para pessoas de qualquer parte do mundo.

  - https://github.com/puppet-br/pcp
  - https://github.com/puppet-br/pcp-controlrepo

Já fizemos a homologação das versões mais recentes do puppet nesta release:

  - Puppet Server 2.3.2
  - Puppet Agent 1.4.2
  - Mcollective 2.8.8
  - PuppetDB 4.0.2
  - PostgreSQL 9.4.6
  - Puppet Explorer 2.0.0
  - ActiveMQ 5.13.2

  Agradeço por todas as ideias e por todas as contribuições recebidas no GitHub, agradeço pela turma que participou do Meetup PCP em Brasília e por todos que ajudaram a testar a nova versão.
  [s]<br>
  Guto
