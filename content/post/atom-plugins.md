+++
categories = ['puppet','atom','planeta']
date = "2016-05-26T20:33:30-03:00"
description = ""
keywords = []
title = "Plugins que uso no ATOM"

+++

Sempre me perguntam quais plugins eu uso no ATOM para desenvolvimento Puppet, segue a lista:

De linguagem:

- language-puppet do atom
- language-yaml do atom
- language-ruby do tom

De linter:

- linter do atom-community 
- linter-puppet-lint do atom-community
- linter-puppet-parser do asquelt
- linter-ruby do atomcommunity

De align:

- aligner do adrianlee44
- aligner-puppet do bigbrozer
- aligner-ruby do adrianlee44

Outros:

- file-icons do DanBrooker
- git-control do jacogr
- open-recent do zren
- minimap do atom-minimap
- remote-edit do sveale

Normalmente monto - via sshfs - o diretório com o código que está em alguma VM de desenvolvimento e trabalho no conforto do ATOM.

Meus alunos preferem que eu mostre o código no ATOM e eles sempre falam muito bem do linter.

\#ficaadica

Se você tiver dica de plugins pro ATOM manda nos comentários ;)

[s]<br>
Guto