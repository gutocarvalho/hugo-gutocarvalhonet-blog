+++
categories = ['puppet','meetup','planeta']
date = "2016-03-28T10:26:02-03:00"
title = "Meetup Puppet-BR em BSB Hackaton"

+++

Foi marcado um meetup Puppet em Brasília no dia 03 de Abril.

Este meetup é mais técnico, vai ser estilo hackaton, vamos nos reunir em uma grande mesa e contribuir com o projeto PCP (Puppet Community Platform).

* http://github.com/gutocarvalho/pcp

Requisitos:

* Cada um traz seu notebook ou desktop
* Cada um traz o que desejar beber ou comer

Recursos:

* Mesas e cadeiras
* Internet 120 Mbits Wifi

Atividades planejadas:

* Homologar última versão do Puppet Server com o PCP;
* Homologar última versão do Puppet Agent com o PCP;
* Homologar última versão do PuppetDB;
* Homologar Puppet Explorer com versão recente do * PuppetDB;
* Homologar última versão do ActiveMQ (repo externo);
* Iniciar estudo de compatibilidade para Debian 8;
* Tradução do README.md para ingles
* Tradução do CHANGELOG para ingles
* Criação de site para PCP com hugo;
* Criação de sub-dominio pcp.infraagil.io.
* Deploy do novo site com hugo
* Planejamento de PCP em docker
* Estudo para merge dos projetos PCP e PCP-M

Faça sua inscrição no site:

* http://www.meetup.com/pt-BR/puppet-br/events/229932603/

Nos vemos lá!

[s]<br>
Guto
