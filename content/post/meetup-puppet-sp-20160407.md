+++
categories = ['puppet','meetup','planeta']
date = "2016-03-28T10:24:51-03:00"
title = "Meetup Puppet-BR em Sao Paulo"

+++

Foi marcado um meetup Puppet em São Paulo no dia 07 de Abril.

Programação:

* Introdução DevOps (20 min) - Guto
* Introdução Infraestrutura como código (20 min) - Daniel
* Introdução Puppet & Puppet 4 (20 min) - Miguel
* Demo de instalação de ambiente Puppet Community (15 min) - Guto
* Demo de instalação do Wordpress usando Puppet (15 min) - Miguel
* Bate papo aberto sobre DevOps, IAC, CM e Puppet (30 min) - Todos

Faça sua inscrição no site:

* http://www.meetup.com/pt-BR/puppet-br/events/229839747/

Nos vemos lá!

[s]<br>
Guto
