+++
categories = ['puppet','meetup','planeta']
date = "2016-05-26T20:32:45-03:00"
description = ""
keywords = []
title = "Resultado do Meetup Puppet DF 20160518"

+++

O Meetup foi execelente, tivemos cerca de noventa inscritos, compareceram cerca de sessenta pessoas, se contarmos a turma da organização e os palestantres acho que tivemos umas setenta pessoas presentes.

Meus agradecimentos à faculdade evangélica, em especial do Prof. Maurício e ao Prof. Dirceu que cederam espaço e nos ajudaram na logística do encontro.

Não conseguimos apresentar do jeito que queríamos o último trabalho (Integração de GitLab com Puppet) devido ao tempo que ficou apertado, mas conseguimos avançar com conteúdo de Puppet para a comunidade local.

A experiência nos mostrou que é melhor reduzir os temas nos próximos encontros, portanto, o próximo meetup terá tema específico, seja desenvolvimento de módulos, seja testes, seja pcp, apenas um tema que iremos desenvolver do início ao fim.

Agradeço aos palestrantes Adriano, Taciano, Rafael e Dirceu pelo tempo que investiram no encontro e por terem compartilhado experiência e conhecimento conosco.

Estamos estudando fazero próximo agora em junho, aguardo sugestões de temas, ideias e quem sabe uma nova parceria para hospedar o Meetup.

## Fotos

Acesse as fotos no link abaixo:

    https://www.flickr.com/photos/puppet-br/albums/72157666275683673

## Slides

Os slides estão no speakerdeck:

1. https://speakerdeck.com/gutocarvalho/meetup-puppet-br-20160518-intro-puppet

2. https://speakerdeck.com/gutocarvalho/meetup-puppet-br-20160518-integracao-entre-puppet-e-vagrant

3. https://speakerdeck.com/gutocarvalho/meetup-puppet-br-20160518-projeto-pcp

4. https://speakerdeck.com/gutocarvalho/meetup-puppet-br-20160518-desenvolvendo-modulos-e-fatos-puppet

5. https://speakerdeck.com/gutocarvalho/meetup-puppet-br-20160518-testes-de-codigo-puppet

[s]<br>
Guto