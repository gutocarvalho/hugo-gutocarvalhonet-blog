+++
categories = ['cotidiano','eventos','planeta']
date = "2016-05-26T20:32:59-03:00"
description = ""
keywords = []
title = "Estarei no TDC SP 2016 :)"

+++

Fui convidado para ajudar a coordenar a trilha de "Infra Ágil" no "The Developers Conference São Paulo" que vai acontecer em Julho de 2016. 

Devo apresentar uma palestra de nome homônimo no evento. A trilha deve acontecer no dia 08 de Julho, ainda não está definido o horário, aviso por aqui assim que souber.

A chamada de trabalhos ainda está aberta, mande sua proposta até o dia 4 de Junho.

    http://www.thedevelopersconference.com.br/tdc/2016/saopaulo/call4papers

Vejo vocês lá!

[s]<br>
Guto


