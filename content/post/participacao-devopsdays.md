+++
categories = ['cotidiano','eventos','planeta']
date = "2016-05-26T20:33:05-03:00"
description = ""
keywords = []
title = "Estarei no DevOpsDays 2016 em Porto Alegre"

+++

Galera fui convidado para apresentar alguns cases de automação de infraestrutura no DevOpsDays de Porto Alegre, será no dia 09 de julho de 2016. Ainda não sei o horário, mas fica a dica do evento.

Faça sua inscrição!

    http://www.devopsdays.org/events/2016-portoalegre/welcome/

Vejo vocês lá!

[s]<br>
Guto


