+++
categories = ['devops','planeta']
date = "2016-06-02T16:41:51-03:00"
description = ""
keywords = []
title = "Cinco anos de DevOps"

+++

Duas palestras para demonstrar o atual estado do movimento e da cultura DevOps nos últimos 5 anos, uma do John Willis (criador da metodologia CAMS) e outra do Patrick Debois (criador do devopsdays).

<iframe width="660" height="330" src="https://www.youtube.com/embed/8rM8lYaMVBE" frameborder="0" allowfullscreen></iframe>

<iframe width="660" height="330" src="https://www.youtube.com/embed/uRMV6tT_mu0" frameborder="0" allowfullscreen></iframe>

Ambas dão uma noção de onde veio e como está o movimento hoje.

Enjoy!

[s]<br>
Guto