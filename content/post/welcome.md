+++
categories = ['cotidiano']
date = "2016-03-27T08:33:47-03:00"
description = ""
keywords = []
title = "Olá Hugo!"

+++

Seja vem vindo ao novo blog do gutocarvalho.net.

Infelizmente o projeto octopress tomou rumos inesperados e iniciei a busca por
outra ferramenta, depois de muitos testes me identifiquei com o hugo, um projeto
novo, com uma comunidade interessada e um desenvolvedor bastante ativo.

Começando 2016 com tudo novo, vem comigo!

[s]<br>
Guto
